install:
	pip install --upgrade .

style:
	black --line-length 119 --target-version py38 .
	isort .

test:
	python -m unittest discover tests "test_*.py"