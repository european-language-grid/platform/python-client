# Changelog

## November 2024 - release 0.6.0

CHANGED:

- Now compatible with both v1 and v2 of Pydantic, making it easier to install this package alongside other packages that require Pydantic v2

## June 2023 - release 0.5.0

CHANGED:

- Switched Dockerfile template to use numeric user and group IDs, for better compatibility with the `runAsNonRoot` security context mechanism in Kubernetes !97

FIXED:

- updated FlaskService/QuartService JSON serialization to be compatible with latest Flask/Quart !98
  - this raises the minimum supported Flask version to 2.2 and Quart to 0.18.0

## June 2022

ADDED:

- default caching in Flask and QuartService classes !94
- better auto-content methods for the Pipeline classe !95

## March 2022

ADDED:

- various improvements to the local installation feature !84
- support of temp-storage in the local installation !85
- support for services taking image as input !86
- ability to customize the URL path in FlaskService/QuartService !87
- allow envvars in local installation !90
- docstring to local installation !91

CHANGED:

- improve recursive messages to use the proper types !83 !88
- replace the base docker image of the FlaskService/QuartService based ELG-compatible service !92

FIXED:

- remove default service type to Flask in cli !89

## January 2022

ADDED:

- `LocalInstallation` class which allows users to generate all the configuation files to deploy ELG-compatible services locally using `docker-compose`
- CLI commands `elg local-installation` to use the `LocalInstallation` class
- `from_local_installation` class method for the `Service` class to call the services deployed locally. This replaces the `from_docker_image` class
- Basic tests for the `Catalog`, the `Entity`, the `Service`, and the `Corpus` classes

CHANGED:

- Update the `Catalog` search parameters to match the new ones of the catalogue
- Update the `map_metadatarecord_to_result` to match the new ELG resources with a more permissive metadata record (tagged as "for information")
- Exclude `null` values from the `elg.model` classes when calling `to_json()`
- Improve the check of the request messages in the `Flask` and the `Quart` classes
- General improvement of the `Failure` messages

FIXED:

- Fix of the `download()` method of the `Corpus` class

## November 2021

CHANGED:

- Add the possibility to get the metadata records with the stats (`display&stat`)
- Ass support of params to `AudioRequest`
- `record` field of the entities is now an object
- Update notebooks
- Several bug fixing

## September 2021

CHANGED:

- `Catalog.search` is now a generator that yields `elg.Entity` instances rather
  than returns a list
- `QuartService` can now stream the audio request without having to store the data locally
- `AudioRequest` has a new `generator` field that can be used to stream the data

FIXED:

- missing exception handler for ConnectException[s] in search API.
